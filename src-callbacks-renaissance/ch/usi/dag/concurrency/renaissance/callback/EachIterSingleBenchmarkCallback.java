package ch.usi.dag.concurrency.renaissance.callback;

import org.renaissance.Plugin;
import org.renaissance.Plugin.BeforeOperationTearDownListener;
import org.renaissance.Plugin.AfterHarnessInitListener;
import org.renaissance.Plugin.AfterOperationSetUpListener;
import ch.usi.dag.tgp.util.ProfileToggle;

public class EachIterSingleBenchmarkCallback implements Plugin, AfterHarnessInitListener, AfterOperationSetUpListener, BeforeOperationTearDownListener {


	@Override
	public void afterHarnessInit() {
		ProfileToggle.disableProfiling();		
	}


	@Override
	public void beforeOperationTearDown(String benchmark, int opIndex, long durationNanos) {
		//if(opIndex == 50){
		//	System.out.println("tEnd: " + System.nanoTime());
		//}
		ProfileToggle.disableProfiling();
	}

	@Override
	public void afterOperationSetUp(String benchmark, int opIndex, boolean isLastOp) {
		if(isLastOp){
			//System.out.println("Profiling enabled at iteration: " + opIndex);
			ProfileToggle.enableProfiling();
			//System.out.println("tInit: " + System.nanoTime());
		}
	}

}
