#!/bin/bash

#Contains user-customizable variables to profile a custom application with tgp

#Automatically exports the variables set in this file
set -a

RENAISSANCE_PATH=$ROOT_PATH/renaissance
RENAISSANCE_JAR=$RENAISSANCE_PATH/renaissance-gpl-0.11.0.jar
RENAISSANCE_CALLBACK=$PROFILER_PATH/callbacks-renaissance.jar
RENAISSANCE_PLUGIN=ch.usi.dag.concurrency.renaissance.callback.EachIterSingleBenchmarkCallback
RENAISSANCE_MAIN_CLASS="org.renaissance.core.Launcher"
BENCHMARK=par-mnemonics
REPETITIONS=16

#Sets the root path of tgp (optional)
#To profile a target application from a location external to the default tgp directory: 
#<ant -buildfile <path/to/tgp/build.xml> <profile-task-bc|profile-task-rc|profile-cc>
#Uncomment to change root path of tgp
#ROOT_PATH="path/to/tgp"

#Sets the classpath of the target application
#Uncomment to change the classpath of the target application
#APP_CLASSPATH="classpath-of-application"
APP_CLASSPATH="$RENAISSANCE_JAR:$PROFILER_PATH/$PROFILER_JAR:$RENAISSANCE_CALLBACK"

#Sets the flags to be sent to the JVM executing the target application, if any 
#Uncomment to change the flags to be sent to the JVM executing the target application
#APP_FLAGS="flags-of-application"
APP_FLAGS="-Dtgp.profile=false"

#Sets target application main class in the target jar file
#Uncomment to change main class
#APP_MAIN_CLASS="MainClass"
APP_MAIN_CLASS=$RENAISSANCE_MAIN_CLASS

#Sets the arguments to be sent to the target application, if the application requires them
#Uncomment to change the arguments to be sent to the target application
#APP_ARGS="arguments-of-application"
APP_ARGS="-r $REPETITIONS --plugin $PROFILER_PATH/$PROFILER_JAR:$RENAISSANCE_CALLBACK!$RENAISSANCE_PLUGIN $BENCHMARK"

#Prevents any further variable to be exported
set +a
